
## Server Manager

Installation

- In the terminal, navigate to root directory
- Run `composer install`
- Execute `php artisan serve`
- It will start the web server (http://127.0.0.1:8000/)
- Get the post man collection from the project docs folder (ServerManager/docs)
- Import to the postman
- You will be getting, apis for getting the servers, comparing them and an api for getting filters.

## Features Available
- Server listing and filtering
- Comparing servers
- Get all available filters
- Caching of the file.
    - If File is modified, cache will clear and then update with new data
- Repository pattern: We could change the implementation to other database without touching controller and service
- Unit Testing : Test coverage for `ServerExcelRepository` is implemented. Refer `tests\Unit\app\Repositories\Server\ServerExcelRepositoryTest.php`
- Run `php artisan test` or `.\vendor\bin\phpunit` for testing

## Notes
- Given excel file is stored under the path `storage\app\LeaseWeb_servers_filters_assignment.xlsx`
- If there any issue in running the app, make sure all the file's lines ending is in LF. If not, you can change this to LF using VS code editor (right bottom corner of the VS code interface)
