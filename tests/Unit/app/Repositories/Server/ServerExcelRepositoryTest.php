<?php

namespace Tests\Unit;

use App\Files\FileManager;
use App\Filters\ServerFilter\ServerFilterManager;
use App\Repositories\Server\ServerExcelRepository;
use Mockery;
use Tests\TestCase;

class ServerExcelRepositoryTest extends TestCase
{
    protected $fileManagerMock = null;
    protected $serverFilterManager = null;
    protected function setUp(): void
    {
        parent::setUp();
        $this->fileManagerMock = Mockery::mock(FileManager::class);
        $this->serverFilterManager = Mockery::mock(ServerFilterManager::class);
        \Illuminate\Support\Facades\Config::set('database', [
            'connections' => [
                'excel' => ['url' => 'dummy']
            ]
        ]);
    }
    
    public function testGetServerByAttributesForSuccess(): void
    {
        $fileManagerMock = $this->fileManagerMock;
        $fileManagerMock->shouldReceive('read')->andReturn([[
            "model" => "Dell R210Intel Xeon X3440",
            "ram" => "16GBDDR3",
            "hdd" => "2x2TBSATA2",
            "location" => "AmsterdamAMS-01",
            "price" => "€49.99"
        ], [
            "model" => "Dell R210Intel Xeon X3440",
            "ram" => "16GBDDR3",
            "hdd" => "2x2TBSATA2",
            "location" => "AmsterdamAMS-01",
            "price" => "€49.99"
        ]]);
        $this->serverFilterManager->shouldReceive('filter')->andReturn(collect([[
            "model" => "Dell R210Intel Xeon X3440",
            "ram" => "16GBDDR3",
            "hdd" => "2x2TBSATA2",
            "location" => "AmsterdamAMS-01",
            "price" => "€49.99"
        ], [
            "model" => "Dell R210Intel Xeon X3440",
            "ram" => "16GBDDR3",
            "hdd" => "2x2TBSATA2",
            "location" => "AmsterdamAMS-01",
            "price" => "€49.99"
        ]]));

        $serverExcelRepository = new ServerExcelRepository($fileManagerMock, $this->serverFilterManager);
        $servers = $serverExcelRepository->getServerByAttributes([]);
        $this->assertEquals(
            $servers[0]['model'],
            'Dell R210Intel Xeon X3440'
        );
    }
}
