<?php

namespace App\Filters\ServerFilter;

use App\Services\ServerFilterService;
use App\Services\ServerService;

class LocationFilter extends AbstractServerFilter
{   
    public $filterField = ServerFilterService::FILTER_HDD_LOCATION;
    public $serverField = ServerService::SERVER_FIELD_LOCATION;
}
