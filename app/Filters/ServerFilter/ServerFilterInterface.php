<?php

namespace App\Filters\ServerFilter;

use Illuminate\Support\Collection;

interface ServerFilterInterface
{
    public function filter(Collection $collection, array $attributes): Collection;
}
