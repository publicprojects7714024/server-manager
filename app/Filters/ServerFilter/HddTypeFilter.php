<?php

namespace App\Filters\ServerFilter;

use App\Services\ServerFilterService;
use App\Services\ServerService;

class HddTypeFilter extends AbstractServerFilter
{
    public $filterField = ServerFilterService::FILTER_HDD_TYPE;
    public $serverField = ServerService::SERVER_FIELD_HDD;
}
