<?php

namespace App\Filters\ServerFilter;

use App\Filters\ServerFilter\ServerFilterInterface;
use App\Services\ServerFilterService;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\App;

class ServerFilterManager
{
    protected ?Collection $serverCollection = null;

    public function filter(Collection $serverCollection, $attributes): Collection
    {
        $this->serverCollection = $serverCollection;
        $availableFilters = ServerFilterService::AVAILABLE_FILTERS;

        foreach($availableFilters as $filterKey) {

            $filter = App::make(ServerFilterInterface::class.$filterKey);
            $this->serverCollection = $filter->filter($this->serverCollection, $attributes);
        }

        return $this->serverCollection;
    }
}
