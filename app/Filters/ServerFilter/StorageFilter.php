<?php

namespace App\Filters\ServerFilter;

use App\FieldFormatter\FilterFieldFormatter\StorageFieldConvertToGb;
use App\FieldFormatter\ServerFieldFormatter\HddFieldConvertToGb;
use App\Filters\ServerFilter\ServerFilterInterface;
use App\Models\Server;
use App\Services\ServerFilterService;
use App\Services\ServerService;
use Illuminate\Support\Collection;

class StorageFilter extends AbstractServerFilter
{
    public $filterField = ServerFilterService::FILTER_STORAGE;
    public $serverField = ServerService::SERVER_FIELD_HDD;

    /** @var StorageFieldConvertToGb */
    protected $storageFieldConvertToGb = null;

    /** @var HddFieldConvertToGb */
    protected $hddFieldConvertToGb = null;

    public function __construct(StorageFieldConvertToGb $storageFieldConvertToGb, HddFieldConvertToGb $hddFieldConvertToGb)
    {
        $this->storageFieldConvertToGb = $storageFieldConvertToGb;
        $this->hddFieldConvertToGb = $hddFieldConvertToGb;
    }
    protected function getStorageFieldConvertToGb(): StorageFieldConvertToGb
    {
        return $this->storageFieldConvertToGb;
    }
    protected function getHddFieldConvertToGb(): HddFieldConvertToGb
    {
        return $this->hddFieldConvertToGb;
    }
    public function filter(Collection $serverCollection, $attributes): Collection
    {
        $storage = $attributes[$this->filterField] ?? null;

        if (empty($storage)) {
            return  $serverCollection;
        }
        $startValue = $storage[0] ?? 0;
        $endValue = $storage[1] ?? 0;

        if (empty($startValue) && empty($endValue)) {
            return  $serverCollection;
        }

        return $serverCollection->filter(function (Server $server, int $key) use ($startValue, $endValue) {

            $hdd = $this->getHddFieldConvertToGb()->format($server->{$this->serverField});
           
            return $hdd >= $startValue && $hdd <= $endValue;
        });
    }
}
