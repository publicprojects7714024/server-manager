<?php

namespace App\Filters\ServerFilter;

use App\Filters\ServerFilter\ServerFilterInterface;
use App\Models\Server;
use Illuminate\Support\Collection;

abstract class AbstractServerFilter implements ServerFilterInterface
{
    public $filterField = null;
    public $serverField = null;

    public function filter(Collection $serverCollection, $attributes): Collection
    {
        return $serverCollection->filter(function (Server $server, int $key) use ($attributes) {
            $fieldValue = $attributes[$this->filterField] ?? null;
            if (!empty($fieldValue)) {
                return str_contains($server->{$this->serverField}, $fieldValue);
            }
            return true;
        });
    }
}
