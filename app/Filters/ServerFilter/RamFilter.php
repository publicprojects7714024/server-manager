<?php

namespace App\Filters\ServerFilter;

use App\FieldFormatter\FilterFieldFormatter\RamFieldConvertToGb;
use App\Models\Server;
use App\Services\ServerFilterService;
use App\Services\ServerService;
use Illuminate\Support\Collection;

class RamFilter extends AbstractServerFilter
{
    public $filterField = ServerFilterService::FILTER_RAM;
    public $serverField = ServerService::SERVER_FIELD_RAM;
    /** @var RamFieldConvertToGb */
    public $ramFieldConvertToGb = null;

    public function __construct(RamFieldConvertToGb $ramFieldConvertToGb)
    {
        $this->ramFieldConvertToGb = $ramFieldConvertToGb;
    }

    public function filter(Collection $serverCollection, $attributes): Collection
    {
        return $serverCollection->filter(function (Server $server, int $key) use ($attributes) {
            
            $rams = $attributes[$this->filterField] ?? [];

            if(empty($rams)) {
                return true;
            }

            foreach($rams as $key => $ramValue) {
                $rams[$key] = $this->ramFieldConvertToGb->format($ramValue);
            }
            $fieldValue = $this->ramFieldConvertToGb->format($server->{$this->serverField});
            if (!empty($fieldValue)) {
                return in_array($fieldValue, $rams);
            }
            return true;
        });
    }
}
