<?php

namespace App\UnitConverters\MemorySizeConverters;

use App\UnitConverters\MemorySizeConverters\MemorySizeConverterInterface;

class GbToGbConverter implements MemorySizeConverterInterface
{
    public const UNIT = 1;

    public function convert(int $size): int
    {
        return $size * self::UNIT;
    }
}
