<?php

namespace App\UnitConverters\MemorySizeConverters;

use App\UnitConverters\MemorySizeConverters\MemorySizeConverterInterface;

class TbToGbConverter implements MemorySizeConverterInterface
{
    public const UNIT = 1024;

    public function convert(int $size): int
    {
        return $size * self::UNIT;
    }
}

