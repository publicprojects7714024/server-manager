<?php

namespace App\UnitConverters\MemorySizeConverters;

interface MemorySizeConverterInterface
{
    public const UNIT_GB = 'gb';
    public const UNIT_TB = 'tb';
    public const UNITS = [
        self::UNIT_GB,
        self::UNIT_TB,
    ];
    public function convert(int $size): int;
}
