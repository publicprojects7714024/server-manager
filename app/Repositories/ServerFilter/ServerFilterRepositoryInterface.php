<?php

namespace App\Repositories\ServerFilter;

interface ServerFilterRepositoryInterface
{
    public function findAll();
}
