<?php

namespace App\Repositories\ServerFilter;

use App\FieldFormatter\FilterFieldFormatter\StorageFieldConvertToGb;
use App\Files\FileManager;
use App\Models\Server;
use App\Models\ServerFilter;
use App\Services\ServerFilterService;
use InvalidArgumentException;
use Illuminate\Support\Collection;

class ServerFilterExcelRepository implements ServerFilterRepositoryInterface
{
    public const START_COLUMN = 'D';
    public const END_COLUMN = 'I';
    public const COLUMN_TO_FIELD_MAPPER = [
        'D' => 'location',
        'I' => 'values',
    ];
    public const CACHE_KEY_SERVERS = 'server_filters';
    public const CHUNK_SIZE = 20;
    public const CONFIG = [
        'startCol' => self::START_COLUMN,
        'endCol' => self::END_COLUMN,
        'chunkSize' => self::CHUNK_SIZE,
        'colToFieldMapper' => self::COLUMN_TO_FIELD_MAPPER,
        'cacheKey' => self::CACHE_KEY_SERVERS
    ];

    /** @var FileManager */
    protected $fileManager = null;
    /** @var StorageFieldConvertToGb */
    protected $storageFieldConvertToGb = null;
    public function __construct(FileManager $fileManager, StorageFieldConvertToGb $storageFieldConvertToGb)
    {
        $this->fileManager = $fileManager;
        $this->storageFieldConvertToGb = $storageFieldConvertToGb;
    }

    public function getFileManager(): FileManager
    {
        return $this->fileManager;
    }
    public function getStorageFieldConvertToGb(): StorageFieldConvertToGb
    {
        return $this->storageFieldConvertToGb;
    }

    /**
     * Get the servers by attributes
     * 
     * @param array $attributes
     * @return $attributes
     */
    public function findAll(): Collection
    {
        $databaseConfig = config('database');

        $filePath = $databaseConfig['connections']['excel']['url'] ?? null;
        if (empty($filePath)) {
            throw new InvalidArgumentException('Invalid Excel File Provided');
        }
        $servers = $this->getFileManager()->read($filePath, self::CONFIG);
        $result = [];
        $locations = [];
        $otherFilters = [];
        foreach ($servers as $oneServerInfo) {
            $server = new ServerFilter();
            foreach ($oneServerInfo as $field => $value) {
                $value = trim($value);
                if (!empty($value)) {
                    if ($field == 'location') {
                        $locations[] = $value;
                    }
                    if ($field == 'values') {
                        $otherFilters[] = $value;
                    }
                }
            }
            $result[] = $server;
        }
       
        $storage = $otherFilters[1];
        $storages = $this->format($storage);

        foreach($storages as $key => $storage) {
            $storages[$key] = $this->getStorageFieldConvertToGb()->format($storage);
        }

        $ram = $otherFilters[2];
        $rams = $this->format($ram);

        $hddType = $otherFilters[3];
        $hddTypes = $this->format($hddType);

        $filters = [
            ServerFilterService::FILTER_STORAGE => $storages,
            ServerFilterService::FILTER_RAM => $rams,
            ServerFilterService::FILTER_HDD_TYPE => $hddTypes,
            ServerFilterService::FILTER_HDD_LOCATION => array_values(array_unique($locations)),
        ];
      
        return collect($filters);
    }

    protected function format($filter)
    {
        $filters = explode(',', $filter);

        $filters = array_map(function ($val) {
            return trim($val);
        }, $filters);

        $filters = array_filter($filters, function ($val) {
            return !empty($val);
        });

        $filters = array_unique($filters);
        $filters = array_values($filters);
        return $filters;
    }
}
