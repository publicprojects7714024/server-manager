<?php

namespace App\Repositories\Server;

use App\Files\FileManager;
use App\Models\Server;
use App\FieldFormatter\FilterFieldFormatter\StorageFieldConvertToGb;
use App\FieldFormatter\ServerFieldFormatter\HddFieldConvertToGb;
use App\Filters\ServerFilter\ServerFilterManager;
use InvalidArgumentException;
use Illuminate\Support\Collection;

class ServerExcelRepository implements ServerRepositoryInterface
{
    public const START_COLUMN = 'A';
    public const END_COLUMN = 'E';
    public const COLUMN_TO_FIELD_MAPPER = [
        'A' => 'model',
        'B' => 'ram',
        'C' => 'hdd',
        'D' => 'location',
        'E' => 'price',
    ];
    public const CACHE_KEY_SERVERS = 'servers';
    public const CHUNK_SIZE = 20;
    public const CONFIG = [
        'startCol' => self::START_COLUMN,
        'endCol' => self::END_COLUMN,
        'chunkSize' => self::CHUNK_SIZE,
        'colToFieldMapper' => self::COLUMN_TO_FIELD_MAPPER,
        'cacheKey' => self::CACHE_KEY_SERVERS
    ];

    /** @var FileManager */
    protected $fileManager = null;

     /** @var StorageFieldConvertToGb */
     protected $storageFieldConvertToGb = null;

      /** @var ServerFilterManager */
    protected $serverFilterManager = null;

    public function __construct(FileManager $fileManager, ServerFilterManager $serverFilterManager)
    {
        $this->fileManager = $fileManager;
        $this->serverFilterManager = $serverFilterManager;
    }

    public function getFileManager(): FileManager
    {
        return $this->fileManager;
    }

    public function findAll(): Collection
    {
        $databaseConfig = config('database');

        $filePath = $databaseConfig['connections']['excel']['url'] ?? null;
        if (empty($filePath)) {
            throw new InvalidArgumentException('Invalid Excel File Provided');
        }
        $servers = $this->getFileManager()->read($filePath, self::CONFIG);
        $result = [];
        $id = 1;
        foreach ($servers as $oneServerInfo) {
            $server = new Server();
            $server->id = $id;
            foreach ($oneServerInfo as $field => $value) {
                $server->{$field} = $value;
            }
            $result[] = $server;
            $id++;
        }
        return collect($result);
    }

    /**
     * Get the servers by attributes
     * 
     * @param array $attributes
     * @return $attributes
     */
    public function compare(array $attributes): Collection
    {
        $serverCollection = $this->findAll();

        return $serverCollection->filter(function (Server $server, int $key) use ($attributes) { 
           return in_array( $server->id, $attributes['id']) ;
        })->values();
       
    }

    /**
     * Get the servers by attributes
     * 
     * @param array $attributes
     * @return $attributes
     */
    public function getServerByAttributes(array $attributes): Collection
    {
        $serverCollection = $this->findAll();
        return $this->filterServerCollection($serverCollection,  $attributes);
    }

    /**
     * Filter the collection
     * 
     * @param Collection $serverCollection
     * @param array $attributes
     * @return Collection
     */
    public function filterServerCollection($serverCollection, $attributes): Collection
    {
        $serverCollection = $this->serverFilterManager->filter($serverCollection, $attributes);
        return $serverCollection->values();
    }
}
