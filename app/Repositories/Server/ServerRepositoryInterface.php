<?php

namespace App\Repositories\Server;

interface ServerRepositoryInterface
{
    public function getServerByAttributes(array $attributes);
    public function compare(array $attributes);
}
