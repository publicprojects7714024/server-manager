<?php

namespace App\Http\Controllers;

use App\Http\Resources\ServerCollection;
use App\Http\Validators\ServerFilterValidator;
use App\Services\ServerService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ServerController extends Controller
{

    /** @var ServerService */
    protected $serverService;
    /** @var ServerFilterValidator */
    protected $serverFilterValidator;
    public function __construct(ServerService $serverService, ServerFilterValidator $serverFilterValidator)
    {
        $this->serverService = $serverService;
        $this->serverFilterValidator = $serverFilterValidator;
    }

    public function getServerService(): ServerService
    {
        return $this->serverService;
    }

    public function getServerFilterValidator(): ServerFilterValidator
    {
        return $this->serverFilterValidator;
    }

    /**
     * List of the servers.
     */
    public function index(Request $request)
    {
        $attributes = $request->all();
        $isValid = $this->getServerFilterValidator()->validate($attributes);

       
        if (empty($isValid)) {
            return response()->json([
                'errors' => [
                    "status" => Response::HTTP_UNPROCESSABLE_ENTITY,
                    "source" => [
                        "pointer" => ""
                    ],
                    "title" =>  ServerFilterValidator::VALIDATION_MSG_INVALID_ATTR,
                    "detail" => ServerFilterValidator::VALIDATION_MSG,
                ]
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $data = $this->getServerService()->getServerByAttributes($attributes);
        return response()->json($data);
    }

    public function compare(Request $request)
    {
        $attributes = $request->all();
        $data = $this->getServerService()->compare($attributes);
        return response()->json($data);
    }
}
