<?php

namespace App\Http\Controllers;

use App\Http\Resources\ServerFilterCollection;
use App\Services\ServerFilterService;

class ServerFilterController extends Controller
{

    /** @var ServerFilterService */
    protected $serverFilterService;

    public function __construct(ServerFilterService $serverFilterService)
    {
        $this->serverFilterService = $serverFilterService;
    }

    public function getServerFilterService(): ServerFilterService
    {
        return $this->serverFilterService;
    }
    /**
     * List of the server filters.
     */
    public function index()
    {
        $data = $this->getServerFilterService()->findAll();
        return new ServerFilterCollection($data);
    }
}
