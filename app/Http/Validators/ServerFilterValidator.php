<?php

namespace App\Http\Validators;

use App\Services\ServerService;

class ServerFilterValidator
{
    public const VALIDATION_MSG_INVALID_ATTR = 'Invalid Attribute';
    public const VALIDATION_MSG = 'Invalid Filters Provided';
    /**
     * Checks the submitted filter is valid
     */
    public function validate($filters)
    {
        if (empty($filters)) {
            return true;
        }
        $availableFilters = ServerService::AVAILABLE_FILTERS;
        $submittedFilters = array_keys($filters);
        foreach ($submittedFilters as $oneFilter) {
            if (!in_array($oneFilter, $availableFilters)) {
                return false;
            }
        }
        return true;
    }
}
