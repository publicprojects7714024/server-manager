<?php

namespace App\Services;

use App\Repositories\ServerFilter\ServerFilterRepositoryInterface;

class ServerFilterService
{
    public const FILTER_STORAGE = 'storage';
    public const FILTER_RAM = 'ram';
    public const FILTER_HDD_TYPE = 'hddType';
    public const FILTER_HDD_LOCATION = 'location';

    public const AVAILABLE_FILTERS = [
        self::FILTER_STORAGE,
        self::FILTER_RAM,
        self::FILTER_HDD_TYPE,
        self::FILTER_HDD_LOCATION
    ];
    /** @var ServerFilterRepositoryInterface */
    protected $serverFilterRepository = null;
    public function __construct(ServerFilterRepositoryInterface $serverFilterRepository)
    {
        $this->serverFilterRepository = $serverFilterRepository;
    }

    protected function getServerFilterRepository(): ServerFilterRepositoryInterface
    {
        return $this->serverFilterRepository;
    }

    public function findAll()
    {
        return $this->getServerFilterRepository()->findAll();
    }
}
