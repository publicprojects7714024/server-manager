<?php

namespace App\Services;

use App\Repositories\Server\ServerRepositoryInterface;

class ServerService
{

    public const SERVER_FIELD_MODEL = 'model';
    public const SERVER_FIELD_RAM = 'ram';
    public const SERVER_FIELD_HDD = 'hdd';
    public const SERVER_FIELD_LOCATION = 'location';
    public const SERVER_FIELD_PRICE = 'price';

    public const AVAILABLE_FILTERS = [
        'storage',
        'ram',
        'hddType',
        'location',
    ];
    /** @var ServerRepositoryInterface */
    protected $serverRepository = null;
    public function __construct(ServerRepositoryInterface $serverRepository)
    {
        $this->serverRepository = $serverRepository;
    }

    protected function getServerRepository(): ServerRepositoryInterface
    {
        return $this->serverRepository;
    }

    public function getServerByAttributes(array $attributes)
    {
        return $this->getServerRepository()->getServerByAttributes($attributes);
    }
    public function compare(array $attributes)
    {
        return $this->getServerRepository()->compare($attributes);
    }
}
