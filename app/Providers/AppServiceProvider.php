<?php

namespace App\Providers;

use App\Filters\ServerFilter\ServerFilterInterface;
use App\Filters\ServerFilter\StorageFilter;
use App\Repositories\Server\ServerExcelRepository;
use App\Repositories\Server\ServerRepositoryInterface;
use App\Repositories\ServerFilter\ServerFilterExcelRepository;
use App\Repositories\ServerFilter\ServerFilterRepositoryInterface;
use App\Services\ServerFilterService;
use App\UnitConverters\MemorySizeConverters\GbToGbConverter;
use App\UnitConverters\MemorySizeConverters\MemorySizeConverterInterface;
use App\UnitConverters\MemorySizeConverters\TbToGbConverter;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [
        ServerRepositoryInterface::class => ServerExcelRepository::class,
        ServerFilterRepositoryInterface::class => ServerFilterExcelRepository::class,
        MemorySizeConverterInterface::class . MemorySizeConverterInterface::UNIT_TB => TbToGbConverter::class,
        MemorySizeConverterInterface::class . MemorySizeConverterInterface::UNIT_GB => GbToGbConverter::class,
    ];

    /**
     * Register any application services.
     */
    public function register(): void
    {
        $availableFilters = ServerFilterService::AVAILABLE_FILTERS;
        foreach ($availableFilters as $filterKey) {
            $className = ucfirst($filterKey);
            $filterClass = sprintf('App\Filters\ServerFilter\%sFilter', $className);
            if(!class_exists($filterClass)) {
                continue;
            }
            $this->app->bind(ServerFilterInterface::class . $filterKey, $filterClass);
        }
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
