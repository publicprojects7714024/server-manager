<?php

namespace App\Files\Readers\Excel\PhpSpreadSheet;

use App\Files\Readers\FileReaderInterface;
use InvalidArgumentException;
use Illuminate\Contracts\Cache\Repository;
use PhpOffice\PhpSpreadsheet\IOFactory;

class PhpSpreadSheetReader implements FileReaderInterface
{
    public const DEFAULT_CHUNK_SIZE = 20;
    public const DEFAULT_START_ROW = 2;
    protected ?ChunkReadFilter $readFilter = null;
    protected array $config = [];
    protected ?Repository $cache = null;
    public function __construct(ChunkReadFilter $readFilter, Repository $cache)
    {
        $this->readFilter = $readFilter;
        $this->cache = $cache;
    }

    public function getConfig(): array
    {
        return $this->config;
    }

    public function setConfig(array $config)
    {
        $this->config = $config;
    }

    public function getReadFilter(): ChunkReadFilter
    {
        return $this->readFilter;
    }
    /**
     * Reads excel file, map the excel columns to entity fields
     * Cache the data if dosen't exist.
     * @param string $fileName
     * @return array
     */
    public function read(string $fileName): array
    {
        $data = [];
        $config = $this->getConfig();
        $startCol = $config['startCol'] ?? null;
        $endCol = $config['endCol'] ?? null;
        $chunkSize = $config['chunkSize'] ?? self::DEFAULT_CHUNK_SIZE;
        $colToFieldMapper = $config['colToFieldMapper'] ?? null;
        $cacheKey = $config['cacheKey'] ?? null;
        $fileModified = $config['fileModified'] ?? null;

        if ($fileModified) {
            $this->cache->forget($cacheKey);
        }

        // Caching
        if (!empty($cacheKey)) {
            $data = $this->cache->get($cacheKey);
            if ($data) {
                return $data;
            }
        }

        if (empty($startCol)) {
            throw new InvalidArgumentException('Config key startCol should be set. Possible values are A, B...');
        }

        if (empty($endCol)) {
            throw new InvalidArgumentException('Config key endCol should be set. Possible values are A, B...');
        }
        $this->getReadFilter()->setStartColumn($startCol);
        $this->getReadFilter()->setEndColumn($endCol);

        $reader = IOFactory::createReaderForFile($fileName);
        $reader->setReadDataOnly(true);
        $spreadsheet =  $reader->load($fileName);
        $highestRow = $spreadsheet->getActiveSheet()->getHighestRow();
        $reader->setReadFilter($this->getReadFilter());

        for ($startRow = self::DEFAULT_START_ROW; $startRow <= $highestRow; $startRow += $chunkSize) {

            $this->getReadFilter()->setRows($startRow, $chunkSize);
            $spreadsheet = $reader->load($fileName);
            $activeRange = $spreadsheet->getActiveSheet()->calculateWorksheetDataDimension();
            $sheetData = $spreadsheet->getActiveSheet()->rangeToArray($activeRange, null, true, true, true);
            $sheetData = array_filter($sheetData, function ($oneSheetData) {
                return array_filter($oneSheetData);
            });
            foreach ($sheetData as $rowNumber => $row) {
                $newRow = [];
                foreach ($row as $key => $val) {
                    $field = $colToFieldMapper[$key] ?? null;
                    if (!empty($field)) {
                        $newRow[$colToFieldMapper[$key]] = $val;
                    }
                }
                $data[] = $newRow;
            }
        }
        if (!empty($cacheKey)) {
            $this->cache->put($cacheKey, $data);
        }
        return $data;
    }
}
