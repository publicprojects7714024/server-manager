<?php

namespace App\Files\Readers\Excel\PhpSpreadSheet;

use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class ChunkReadFilter implements IReadFilter
{
    private $startRow = 0;

    private $endRow = 0;

    private $startColumn = null;

    private $endColumn = null;


    public function getStartColumn(): ?string
    {
        return $this->startColumn;
    }

    public function setStartColumn(?string $startColumn)
    {
        $this->startColumn = $startColumn;
    }

    public function getEndColumn(): ?string
    {
        return $this->endColumn;
    }

    public function setEndColumn(?string $endColumn)
    {
        $this->endColumn = $endColumn;
    }

    /**
     * Set the list of rows that we want to read.
     *
     * @param mixed $startRow
     * @param mixed $chunkSize
     */
    public function setRows($startRow, $chunkSize): void
    {
        $this->startRow = $startRow;
        $this->endRow = $startRow + $chunkSize;
    }

    
    public function readCell($columnAddress, $row, $worksheetName = '')
    {
        //  Only read the rows that are configured in $this->_startRow and $this->_endRow
        if (($row >= $this->startRow && $row < $this->endRow)) {

            if (in_array($columnAddress, range($this->getStartColumn(), $this->getEndColumn()))) {
                return true;
            }
        }

        return false;
    }
}
