<?php
namespace App\Files\Readers;

interface FileReaderInterface
{
    public function read(string $fileName);
}
