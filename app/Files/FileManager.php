<?php

namespace App\Files;

use App\Files\Readers\Excel\PhpSpreadSheet\PhpSpreadSheetReader;
use Illuminate\Contracts\Container\Container;

class FileManager
{
    protected ?Container $container = null;
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function read(string $fileName, array $config)
    {
        return $this->getReader($fileName, $config)->read($fileName);
    }

    public function getReader($fileName, array $config)
    {
        $reader = null;
        $extension = self::getFileExtension($fileName);
        $fileModified = $this->isFileModified($fileName);
        $config['fileModified'] = $fileModified;
        switch ($extension) {
            case 'xlsx':
                /** @var  PhpSpreadSheetReader*/
                $reader = $this->container->make(PhpSpreadSheetReader::class);
                $reader->setConfig($config);
                break;
        }
        return $reader;
    }

    protected function getFileExtension($fileName)
    {
        $pathInfo = pathinfo($fileName);
        return $pathInfo['extension'];
    }

    protected function getStorageFileName($fileName): string | bool
    {
        if (!@file_exists($fileName)) {
            return false;
        }
        $pathInfo = pathinfo($fileName);
       
        return $pathInfo['dirname'] . '\\'. $pathInfo['filename'] . '.txt';
    }

    protected function saveFileModifiedTime(string $fileName): string | bool
    {
        $fileTime = fileatime($fileName);
        $storageFileName = $this->getStorageFileName($fileName);
        if (empty($storageFileName)) {
            return false;
        }
        file_put_contents($storageFileName,  $fileTime);
        return $fileTime;
    }

    public function getLastModifiedTime(string $fileName): string | bool
    {
        $storageFileName = $this->getStorageFileName($fileName);
        if (empty($storageFileName)) {
            return false;
        }
        if (!file_exists($storageFileName)) {
            return false;
        }
        return file_get_contents($storageFileName);
    }

    public function isFileModified(string $fileName): bool
    {
        $lastModifiedTime = $this->getLastModifiedTime($fileName);
        // dump($lastModifiedTime );
        // dump(fileatime($fileName) );
        // die;
        if (!empty($lastModifiedTime)) {
            $this->saveFileModifiedTime($fileName);
            return $lastModifiedTime != fileatime($fileName);
        }
        $this->saveFileModifiedTime($fileName);
        return true;
    }
}
