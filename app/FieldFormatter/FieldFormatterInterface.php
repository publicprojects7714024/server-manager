<?php

namespace App\FieldFormatter;

interface FieldFormatterInterface
{
    public function format($fieldValue);
}
