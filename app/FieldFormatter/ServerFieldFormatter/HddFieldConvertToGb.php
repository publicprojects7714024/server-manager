<?php

namespace App\FieldFormatter\ServerFieldFormatter;

use App\FieldFormatter\FieldFormatterInterface;
use App\UnitConverters\MemorySizeConverters\MemorySizeConverterInterface;
use Illuminate\Support\Facades\App;
use InvalidArgumentException;

class HddFieldConvertToGb implements FieldFormatterInterface
{
    // pattern expected in the excel file
    public const PATTERN = '/(\d+)x(\d+)([TGM]B)(\w+)/i';

    public function format($fieldValue)
    {
        preg_match(self::PATTERN, $fieldValue, $matches);
        if (count($matches) < 5) {
            throw new InvalidArgumentException('Invalid data format provided');
        }
        $quantity = intval($matches[1]);
        $capacity = intval($matches[2]);
        $unit = strtolower($matches[3]);

        if (!in_array($unit, MemorySizeConverterInterface::UNITS)) {
            throw new InvalidArgumentException('Invalid unit provided');
        }
        $converter = App::make(MemorySizeConverterInterface::class . $unit);
        $value = $converter->convert($capacity);
        return $quantity * $value;
    }
}
