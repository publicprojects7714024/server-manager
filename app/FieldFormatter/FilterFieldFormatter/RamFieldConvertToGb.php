<?php

namespace App\FieldFormatter\FilterFieldFormatter;

use App\FieldFormatter\FieldFormatterInterface;
use App\UnitConverters\MemorySizeConverters\MemorySizeConverterInterface;
use Illuminate\Support\Facades\App;
use InvalidArgumentException;

class RamFieldConvertToGb implements FieldFormatterInterface
{
    // pattern expected in the excel file
    public const PATTERN = '/(\d+)([TG]B)/i';

    public function format($fieldValue)
    {
        preg_match(self::PATTERN, $fieldValue, $matches);
       
        if (count($matches) < 2) {
            throw new InvalidArgumentException('Invalid data format provided. '. $fieldValue);
        }
        $capacity = intval($matches[1]);
        $unit = strtolower($matches[2]);

        if (!in_array($unit, MemorySizeConverterInterface::UNITS)) {
            throw new InvalidArgumentException('Invalid unit provided');
        }
        $converter = App::make(MemorySizeConverterInterface::class . $unit);
        $value = $converter->convert($capacity);
        return $value;
    }
}
